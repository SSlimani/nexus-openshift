# Nexus Openshift Template

based on [OpenShiftDemos](https://github.com/OpenShiftDemos/nexus) from 

## Prerequisites

* oc client installed on your machine
* An Openshift project

### What does this template install

It uses the official Docker images of the following items:

* Nexus 3.x Open Source Software
